const puppeteer = require("puppeteer-firefox");
const sgMail = require("@sendgrid/mail");

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

(async () => {
  const browser = await puppeteer.launch({
    defaultViewport: { width: 1920, height: 1080 },
    headless: true
  });

  const page = await browser.newPage();
  await page.goto("https://tesla.com/de_CH/model3/design#battery");

  const linkHandlers = await page.$x("//a[contains(text(), 'Einzelheiten')]");
  if (linkHandlers.length > 0) {
    await linkHandlers[0].click();
  } else {
    console.log("Link not found");
  }

  const interestrate = (await page.content()).match(
    /(?<=Zinssatz von )....%/gi
  );
  const expectedString = "3,40%";
  if (interestrate != expectedString) {
    sgMail.send(message(interestrate));
  }

  await browser.close();
})();

function message(interestrate) {
  const message = {
    to: "josua.bryner@gmail.com",
    from: "teslawatcher@nebulaforge.io",
    subject: "Tesla Leasingzins wurde geändert!",
    html: `Tesla Leasingzins neu bei <strong>${interestrate}</strong>!<br>--> <a href="https://www.tesla.com/de_CH/model3/design#battery">Los! Los! Los!</a><br><br><br><i>-- proudly presented by TeslaWatcher</i>`
  };

  return message;
}
