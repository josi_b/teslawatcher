FROM node:alpine
COPY *.js package.json package-lock.json ./
RUN npm install && npm link
CMD ["/usr/local/bin/teslawatcher"]
